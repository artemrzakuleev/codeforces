import java.util.Scanner;

import static java.lang.System.out;

public class Main4A {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int w=scanner.nextInt();
        if ((w % 4 == 0) || ((w % 4 == 2) && (w >= 4))) {
            out.print("YES");
        } else {
            out.print("NO");
        }
    }
}