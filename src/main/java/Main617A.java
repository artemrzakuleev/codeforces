import java.util.Scanner;

import static java.lang.System.out;

public class Main617A {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int w = scanner.nextInt();
        int result = w / 5;
        w -= result * 5;
        if (w > 0) {
            result ++;
        }
        out.println(result);
    }
}