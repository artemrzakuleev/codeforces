import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static java.lang.System.out;

public class Main469A {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        Set<Integer> a=new HashSet<>();
        int n=scanner.nextInt();
        int p=scanner.nextInt();
        for (int i = 0 ; i < p ; i ++) {
            a.add(scanner.nextInt());
        }
        int y=scanner.nextInt();
        for (int i = 0;i < y;i++) {
            a.add(scanner.nextInt());
        }
        boolean result=true;
        for (int i = 1 ; i <= n ; i ++) {
            if (!a.remove(i)) {
                result=false;
            }
        }
        if (!result) {
            out.println("Oh, my keyboard!");
        }
        else {
            out.println("I become the guy.");
        }
    }
}