import java.util.Scanner;

import static java.lang.System.out;

public class Main71A {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        for (int i = 0; i <= n; i ++) {
            String word=scanner.nextLine();
            if (word.length()<11) {
                out.println(word);
            }
            else {
                String builder = String.valueOf(word.charAt(0)) +
                        (word.length() - 2) +
                        word.charAt(word.length() - 1);
                out.println(builder);
            }
        }
    }
}