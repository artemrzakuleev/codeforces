import java.util.Scanner;

import static java.lang.System.out;

public class Main977A {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int k=scanner.nextInt();
        for (int i = 0;i < k;i ++)   {
            if (n%10==0)    {
                n/=10;
            }
            else n--;
        }
        out.println(n);
    }
}
